USE [master]
GO
/****** Object:  Database [book_web]    Script Date: 05/18/2021 20:18:52 ******/
CREATE DATABASE [book_web] ON  PRIMARY 
( NAME = N'book_web', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\book_web.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'book_web_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\book_web_log.ldf' , SIZE = 1344KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [book_web] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [book_web].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [book_web] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [book_web] SET ANSI_NULLS OFF
GO
ALTER DATABASE [book_web] SET ANSI_PADDING OFF
GO
ALTER DATABASE [book_web] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [book_web] SET ARITHABORT OFF
GO
ALTER DATABASE [book_web] SET AUTO_CLOSE ON
GO
ALTER DATABASE [book_web] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [book_web] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [book_web] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [book_web] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [book_web] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [book_web] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [book_web] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [book_web] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [book_web] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [book_web] SET  ENABLE_BROKER
GO
ALTER DATABASE [book_web] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [book_web] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [book_web] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [book_web] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [book_web] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [book_web] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [book_web] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [book_web] SET  READ_WRITE
GO
ALTER DATABASE [book_web] SET RECOVERY SIMPLE
GO
ALTER DATABASE [book_web] SET  MULTI_USER
GO
ALTER DATABASE [book_web] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [book_web] SET DB_CHAINING OFF
GO
USE [book_web]
GO
/****** Object:  Table [dbo].[translator]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[translator](
	[translatorId] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[linkFanpage] [nvarchar](200) NOT NULL,
	[donationAccount] [nvarchar](100) NOT NULL,
	[imgName] [varchar](100) NULL,
	[status] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[translatorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[category]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[category](
	[categoryId] [int] IDENTITY(1,1) NOT NULL,
	[categoryName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_category] PRIMARY KEY CLUSTERED 
(
	[categoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[account]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[account](
	[accountId] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](200) NOT NULL,
	[password] [nvarchar](200) NOT NULL,
	[status] [int] NOT NULL,
	[role] [int] NOT NULL,
	[activeCode] [varchar](50) NOT NULL,
	[name] [nvarchar](200) NULL,
 CONSTRAINT [PK_account] PRIMARY KEY CLUSTERED 
(
	[accountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[book]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[book](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NOT NULL,
	[authorId] [int] NOT NULL,
	[translatorId] [int] NOT NULL,
	[totalChap] [int] NULL,
	[appear] [int] NULL,
	[totalView] [int] NULL,
	[status] [nvarchar](200) NOT NULL,
	[description] [nvarchar](2000) NULL,
	[imgName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK__book__3213E83F694D560B] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[following]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[following](
	[accountId] [int] NOT NULL,
	[bookId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chapter]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chapter](
	[bookId] [int] NOT NULL,
	[chapterId] [int] IDENTITY(1,1) NOT NULL,
	[chapterName] [nvarchar](250) NOT NULL,
	[dateOfPublic] [date] NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [chapter_prk] PRIMARY KEY CLUSTERED 
(
	[bookId] ASC,
	[chapterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[comment]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[comment](
	[commentId] [int] IDENTITY(1,1) NOT NULL,
	[bookId] [int] NOT NULL,
	[chapterId] [int] NULL,
	[accountId] [int] NOT NULL,
	[content] [nvarchar](500) NOT NULL,
	[date] [date] NOT NULL,
 CONSTRAINT [com_prk] PRIMARY KEY CLUSTERED 
(
	[commentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rating]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rating](
	[accountId] [int] NOT NULL,
	[bookId] [int] NOT NULL,
	[mark] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[book_category]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[book_category](
	[categoryId] [int] NOT NULL,
	[bookId] [int] NOT NULL,
 CONSTRAINT [book_cate_prk] PRIMARY KEY CLUSTERED 
(
	[bookId] ASC,
	[categoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[report]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[report](
	[reportId] [int] NOT NULL,
	[accountId] [int] NULL,
	[bookId] [int] NULL,
	[commentId] [int] NULL,
	[content] [nvarchar](400) NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [PK_report] PRIMARY KEY CLUSTERED 
(
	[reportId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[history]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[history](
	[accountId] [int] NOT NULL,
	[bookId] [int] NOT NULL,
	[chapterId] [int] NOT NULL,
	[date] [date] NOT NULL,
	[historyId] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [history_prk] PRIMARY KEY CLUSTERED 
(
	[historyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[frame]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[frame](
	[bookId] [int] NOT NULL,
	[chapterId] [int] NOT NULL,
	[frameId] [int] IDENTITY(1,1) NOT NULL,
	[imgName] [varchar](100) NOT NULL,
	[status] [int] NOT NULL,
 CONSTRAINT [frame_prk] PRIMARY KEY CLUSTERED 
(
	[bookId] ASC,
	[chapterId] ASC,
	[frameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bookmark]    Script Date: 05/18/2021 20:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bookmark](
	[accountId] [int] NOT NULL,
	[bookId] [int] NOT NULL,
	[chapterId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_translator_status]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[translator] ADD  CONSTRAINT [DF_translator_status]  DEFAULT ((0)) FOR [status]
GO
/****** Object:  Default [DF__book__totalChap__108B795B]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book] ADD  CONSTRAINT [DF__book__totalChap__108B795B]  DEFAULT ((0)) FOR [totalChap]
GO
/****** Object:  Default [DF__book__totalLike__117F9D94]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book] ADD  CONSTRAINT [DF__book__totalLike__117F9D94]  DEFAULT ((0)) FOR [appear]
GO
/****** Object:  Default [DF__book__totalComme__1273C1CD]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book] ADD  CONSTRAINT [DF__book__totalComme__1273C1CD]  DEFAULT ((0)) FOR [totalView]
GO
/****** Object:  Default [DF__book__imgName__1367E606]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book] ADD  CONSTRAINT [DF__book__imgName__1367E606]  DEFAULT ('') FOR [imgName]
GO
/****** Object:  ForeignKey [book_trans_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book]  WITH CHECK ADD  CONSTRAINT [book_trans_frk] FOREIGN KEY([translatorId])
REFERENCES [dbo].[translator] ([translatorId])
GO
ALTER TABLE [dbo].[book] CHECK CONSTRAINT [book_trans_frk]
GO
/****** Object:  ForeignKey [FK_following_account]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[following]  WITH CHECK ADD  CONSTRAINT [FK_following_account] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[following] CHECK CONSTRAINT [FK_following_account]
GO
/****** Object:  ForeignKey [FK_following_book]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[following]  WITH CHECK ADD  CONSTRAINT [FK_following_book] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[following] CHECK CONSTRAINT [FK_following_book]
GO
/****** Object:  ForeignKey [chap_book_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[chapter]  WITH CHECK ADD  CONSTRAINT [chap_book_frk] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[chapter] CHECK CONSTRAINT [chap_book_frk]
GO
/****** Object:  ForeignKey [com_acc_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [com_acc_frk] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [com_acc_frk]
GO
/****** Object:  ForeignKey [com_book_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[comment]  WITH CHECK ADD  CONSTRAINT [com_book_frk] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[comment] CHECK CONSTRAINT [com_book_frk]
GO
/****** Object:  ForeignKey [FK_rating_account]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_account] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_account]
GO
/****** Object:  ForeignKey [FK_rating_book]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[rating]  WITH CHECK ADD  CONSTRAINT [FK_rating_book] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[rating] CHECK CONSTRAINT [FK_rating_book]
GO
/****** Object:  ForeignKey [bookcate_book_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book_category]  WITH CHECK ADD  CONSTRAINT [bookcate_book_frk] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[book_category] CHECK CONSTRAINT [bookcate_book_frk]
GO
/****** Object:  ForeignKey [bookcate_cate_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[book_category]  WITH CHECK ADD  CONSTRAINT [bookcate_cate_frk] FOREIGN KEY([categoryId])
REFERENCES [dbo].[category] ([categoryId])
GO
ALTER TABLE [dbo].[book_category] CHECK CONSTRAINT [bookcate_cate_frk]
GO
/****** Object:  ForeignKey [FK_report_account]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[report]  WITH CHECK ADD  CONSTRAINT [FK_report_account] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[report] CHECK CONSTRAINT [FK_report_account]
GO
/****** Object:  ForeignKey [FK_report_book]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[report]  WITH CHECK ADD  CONSTRAINT [FK_report_book] FOREIGN KEY([bookId])
REFERENCES [dbo].[book] ([id])
GO
ALTER TABLE [dbo].[report] CHECK CONSTRAINT [FK_report_book]
GO
/****** Object:  ForeignKey [FK_report_comment]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[report]  WITH CHECK ADD  CONSTRAINT [FK_report_comment] FOREIGN KEY([commentId])
REFERENCES [dbo].[comment] ([commentId])
GO
ALTER TABLE [dbo].[report] CHECK CONSTRAINT [FK_report_comment]
GO
/****** Object:  ForeignKey [hist_acco_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[history]  WITH CHECK ADD  CONSTRAINT [hist_acco_frk] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[history] CHECK CONSTRAINT [hist_acco_frk]
GO
/****** Object:  ForeignKey [hist_chap_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[history]  WITH CHECK ADD  CONSTRAINT [hist_chap_frk] FOREIGN KEY([bookId], [chapterId])
REFERENCES [dbo].[chapter] ([bookId], [chapterId])
GO
ALTER TABLE [dbo].[history] CHECK CONSTRAINT [hist_chap_frk]
GO
/****** Object:  ForeignKey [fram_chap_frk]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[frame]  WITH CHECK ADD  CONSTRAINT [fram_chap_frk] FOREIGN KEY([bookId], [chapterId])
REFERENCES [dbo].[chapter] ([bookId], [chapterId])
GO
ALTER TABLE [dbo].[frame] CHECK CONSTRAINT [fram_chap_frk]
GO
/****** Object:  ForeignKey [FK_bookmark_account]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[bookmark]  WITH CHECK ADD  CONSTRAINT [FK_bookmark_account] FOREIGN KEY([accountId])
REFERENCES [dbo].[account] ([accountId])
GO
ALTER TABLE [dbo].[bookmark] CHECK CONSTRAINT [FK_bookmark_account]
GO
/****** Object:  ForeignKey [FK_bookmark_chapter]    Script Date: 05/18/2021 20:18:54 ******/
ALTER TABLE [dbo].[bookmark]  WITH CHECK ADD  CONSTRAINT [FK_bookmark_chapter] FOREIGN KEY([bookId], [chapterId])
REFERENCES [dbo].[chapter] ([bookId], [chapterId])
GO
ALTER TABLE [dbo].[bookmark] CHECK CONSTRAINT [FK_bookmark_chapter]
GO
